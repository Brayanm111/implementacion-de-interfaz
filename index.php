<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
</head>
<body>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<LINK REL=StyleSheet HREF="estilos.css" >
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="index.html">Tienda Articulos</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse justify-content-end" id="navbarsExampleDefault">
            <ul class="navbar-nav m-auto">
                <li class="nav-item">
                    <a class="nav-link" href="index.php"> Inicio</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="category.html">Categorias <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="product.html">producto</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="cart.html">carro de compras</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="contact.html">Contacto</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login.php">Iniciar Sesion</a>
                </li>
            </ul>

            <form class="form-inline my-2 my-lg-0">
                <div class="input-group input-group-sm">
                    <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Buscar...">
                    <div class="input-group-append">
                        <button type="button" class="btn btn-secondary btn-number">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
                <a class="btn btn-success btn-sm ml-3" href="cart.html">
                    <i class="fa fa-shopping-cart"></i> Cart
                    <span class="badge badge-light">3</span>
                </a>
            </form>
        </div>
    </div>
</nav>
<section class="jumbotron text-center">
    <div class="container">
        <h1 class="jumbotron-heading">Tienda Web</h1>
        <p class="lead text-muted mb-0">Estos son algunos de los productos que ofrese la tienda virtual entre distintas categorias con distintos productos para poder comprar y obtener todos los que desee el comprador.</p>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    <li class="breadcrumb-item"><a href="category.html">Category</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Sub-category</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-12 col-sm-3">
            <div class="card bg-light mb-3">
                <div class="card-header bg-primary text-white text-uppercase"><i class="fa fa-list"></i> Categorias</div>
                <ul class="list-group category_block">
                <?php
            $Categorias=json_decode(file_get_contents("http://127.0.0.1:8000/PHP-API/Categoria.php"),true);
            for ($i=0; $i<count($Categorias); $i++){
            ?>
                    <li class="list-group-item"><a href="category.html"><?php echo($Categorias[$i]["Nombre"])?></a></li>
            <?php
            }
            ?>  
                </ul>
            </div> 
            <?php
            $datos=json_decode(file_get_contents("http://127.0.0.1:8000/PHP-API/Articulo.php"),true);
            ?>
         <div class="card bg-light mb-3"> 
                <div class="card-header bg-success text-white text-uppercase">Last product</div>
                <div class="card-body">            
           

                    <img class="img-fluid" src="f/asus.jpg" />
                    <h5 class="card-title"><?php echo($datos[1]["Nombre_Articulo"])?></h5>                    

                    <p class="card-text"><?php echo($datos[1]["Descripcion"])?></p>
                    <p class="bloc_left_price"><?php echo($datos[1]["Precio_Articulo"]."$")?> </p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card">
                        <img class="card-img-top" src="f/hp.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h4 class="card-title"><a href="product.html" title="View Product"><?php echo($datos[2]["Nombre_Articulo"])?></a></h4>
                            <p class="card-text"><?php echo($datos[2]["Descripcion"])?></p>
                            <div class="row">
                                <div class="col">
                                    <p class="btn btn-danger btn-block"><?php echo($datos[2]["Precio_Articulo"]."$")?></p>
                                </div>
                                <div class="col">
                                    <a href="#" class="btn btn-success btn-block">Añadir Compra</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card">
                        <img class="card-img-top" src="f/celu1.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h4 class="card-title"><a href="product.html" title="View Product"><?php echo($datos[3]["Nombre_Articulo"])?></a></h4>
                            <p class="card-text"><?php echo($datos[3]["Descripcion"])?></p>
                            <div class="row">
                                <div class="col">
                                    <p class="btn btn-danger btn-block"><?php echo($datos[3]["Precio_Articulo"]."$")?></p>
                                </div>
                                <div class="col">
                                    <a href="#" class="btn btn-success btn-block">Añadir Compra</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card">
                        <img class="card-img-top" src="f/A5.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h4 class="card-title"><a href="product.html" title="View Product"><?php echo($datos[0]["Nombre_Articulo"])?></a></h4>
                            <p class="card-text"><?php echo($datos[0]["Descripcion"])?></p>
                            <div class="row">
                                <div class="col">
                                    <p class="btn btn-danger btn-block"><?php echo($datos[0]["Precio_Articulo"]."$")?></p>
                                </div>
                                <div class="col">
                                    <a href="#" class="btn btn-success btn-block">Añadir Compra</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card">
                        <img class="card-img-top" src="f/Camara.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h4 class="card-title"><a href="product.html" title="View Product"><?php echo($datos[4]["Nombre_Articulo"])?></a></h4>
                            <p class="card-text"><?php echo($datos[4]["Descripcion"])?></p>
                            <div class="row">
                                <div class="col">
                                    <p class="btn btn-danger btn-block"><?php echo($datos[4]["Precio_Articulo"]."$")?></p>
                                </div>
                                <div class="col">
                                    <a href="#" class="btn btn-success btn-block">Añadir Compra</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card">
                        <img class="card-img-top" src="f/razer.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h4 class="card-title"><a href="product.html" title="View Product"><?php echo($datos[5]["Nombre_Articulo"])?></a></h4>
                            <p class="card-text"><?php echo($datos[5]["Descripcion"])?></p>
                            <div class="row">
                                <div class="col">
                                    <p class="btn btn-danger btn-block"><?php echo($datos[5]["Precio_Articulo"]."$")?></p>
                                </div>
                                <div class="col">
                                    <a href="#" class="btn btn-success btn-block">Añadir Compra</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card">
                        <img class="card-img-top" src="f/msi1.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h4 class="card-title"><a href="product.html" title="View Product"><?php echo($datos[6]["Nombre_Articulo"])?></a></h4>
                            <p class="card-text"><?php echo($datos[6]["Descripcion"])?></p>
                            <div class="row">
                                <div class="col">
                                    <p class="btn btn-danger btn-block"><?php echo($datos[6]["Precio_Articulo"]."$")?></p>
                                </div>
                                <div class="col">
                                    <a href="#" class="btn btn-success btn-block">Añadir Compra</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <nav aria-label="...">
                        <ul class="pagination">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1">Previous</a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item active">
                                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#">Next</a>
                            </li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>

    </div>
</div>

<!-- Footer -->
<footer class="text-light">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-4 col-xl-3">
                <h5>About</h5>
                <hr class="bg-white mb-2 mt-0 d-inline-block mx-auto w-25">
                <p class="mb-0">
                    La pagina intenta ofrecer el mejor servicio al usuario que llegue a utilizarla.
                </p>
            </div>

            <div class="col-md-2 col-lg-2 col-xl-2 mx-auto">
                <h5>Informacion</h5>
                <hr class="bg-white mb-2 mt-0 d-inline-block mx-auto w-25">
                <ul class="list-unstyled">
                    <li><a href="">Link 1</a></li>
                    <li><a href="">Link 2</a></li>
                    <li><a href="">Link 3</a></li>
                    <li><a href="">Link 4</a></li>
                </ul>
            </div>

            <div class="col-md-3 col-lg-2 col-xl-2 mx-auto">
                <h5>Otros Links</h5>
                <hr class="bg-white mb-2 mt-0 d-inline-block mx-auto w-25">
                <ul class="list-unstyled">
                    <li><a href="">Link 1</a></li>
                    <li><a href="">Link 2</a></li>
                    <li><a href="">Link 3</a></li>
                    <li><a href="">Link 4</a></li>
                </ul>
            </div>

            <div class="col-md-4 col-lg-5 col-xl-5">
                <h5>Contacto</h5>
                <hr class="bg-white mb-2 mt-0 d-inline-block mx-auto w-25">
                <ul class="list-unstyled">
                    <li><i class="fa fa-home mr-2"></i> My company</li>
                    <li><i class="fa fa-envelope mr-2"></i>brayan_meneses1999@outlook.es</li>
                    <li><i class="fa fa-phone mr-2"></i> + 591 77740092</li>
                    <li><i class="fa fa-print mr-2"></i> No habilitado</li>
                </ul>
            </div>
            <div class="col-12 copyright mt-3">
                <p class="float-left">
                    <a href="#">Volver al Inicio de la pagina </a>
                </p>
                
            </div>
        </div>
    </div>
</footer>
</body>
</html>


