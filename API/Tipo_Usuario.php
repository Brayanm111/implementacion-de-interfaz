<?php
include "config.php";
include "utils.php";


$dbConn =  connect($db);

/*
  listar todos los articulos o solo uno GET
 */
if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
    if (isset($_GET['ID_Usuario']))
    {
      //Mostrar un post
      $sql = $dbConn->prepare("SELECT * FROM tipo_usuario where ID_Usuario=:ID_Usuario");
      $sql->bindValue(':ID_Categoria', $_GET['ID_Categoria']);
      $sql->execute();
      header("HTTP/1.1 200 OK");
      echo json_encode(  $sql->fetch(PDO::FETCH_ASSOC)  );
      exit();
	  }
    else {
      //Mostrar lista de post
      $sql = $dbConn->prepare("SELECT * FROM tipo_usuario");
      $sql->execute();
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      header("HTTP/1.1 200 OK");
      echo json_encode( $sql->fetchAll()  );
      exit();
	}
}

// Crear un nuevo post
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $input = $_POST;
    $sql = "INSERT INTO tipo_usuario
          (Rol)
          VALUES
          (:Rol)";
    $statement = $dbConn->prepare($sql);
    bindAllValues($statement, $input);
    $statement->execute();
    $postId = $dbConn->lastInsertId();
    if($postId)
    {
      $input['ID_Usuario'] = $postId;
      header("HTTP/1.1 200 OK");
      echo json_encode($input);
      exit();
	 }
}

//Borrar
if ($_SERVER['REQUEST_METHOD'] == 'DELETE')
{
	$id = $_GET['ID_Usuario'];
  $statement = $dbConn->prepare("DELETE FROM tipo_usuario where ID_Usuario=:ID_Usuario");
  $statement->bindValue(':ID_Usuario', $id);
  $statement->execute();
	header("HTTP/1.1 200 OK");
	exit();
}

//Actualizar
if ($_SERVER['REQUEST_METHOD'] == 'PUT')
{
    $input = $_GET;
    $postId = $input['ID_Usuario'];
    $fields = getParams($input);

    $sql = "
          UPDATE tipo_usuario
          SET $fields
          WHERE ID_Usuario='$postId'
           ";

    $statement = $dbConn->prepare($sql);
    bindAllValues($statement, $input);

    $statement->execute();
    header("HTTP/1.1 200 OK");
    exit();
}


//En caso de que ninguna de las opciones anteriores se haya ejecutado
header("HTTP/1.1 400 Bad Request");

?>