<?php
include "config.php";
include "utils.php";


$dbConn =  connect($db);

/*
  listar todos los posts o solo uno
 */
if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
    if (isset($_GET['ID_Venta']))
    {
      //Mostrar un post
      $sql = $dbConn->prepare("SELECT * FROM venta where ID_Venta=:ID_Venta");
      $sql->bindValue(':ID_Venta', $_GET['ID_Venta']);
      $sql->execute();
      header("HTTP/1.1 200 OK");
      echo json_encode(  $sql->fetch(PDO::FETCH_ASSOC)  );
      exit();
	  }
    else {
      //Mostrar lista de post
      $sql = $dbConn->prepare("SELECT * FROM venta");
      $sql->execute();
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      header("HTTP/1.1 200 OK");
      echo json_encode( $sql->fetchAll()  );
      exit();
	}
}

// Crear un nuevo post
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $input = $_POST;
    $sql = "INSERT INTO venta
          (ID_Articulo, ID_Cliente, Fecha, Cantidad, Detalle_Venta, Precio_Total)
          VALUES
          (:ID_Articulo, :ID_Cliente, :Fecha, :Cantidad, :Detalle_Venta, :Precio_Total)";
    $statement = $dbConn->prepare($sql);
    bindAllValues($statement, $input);
    $statement->execute();
    $postId = $dbConn->lastInsertId();
    if($postId)
    {
      $input['ID_Venta'] = $postId;
      header("HTTP/1.1 200 OK");
      echo json_encode($input);
      exit();
	 }
}

//Borrar
if ($_SERVER['REQUEST_METHOD'] == 'DELETE')
{
	$id = $_GET['ID_Venta'];
  $statement = $dbConn->prepare("DELETE FROM venta where ID_Venta=:ID_Venta");
  $statement->bindValue(':ID_Venta', $id);
  $statement->execute();
	header("HTTP/1.1 200 OK");
	exit();
}

//Actualizar
if ($_SERVER['REQUEST_METHOD'] == 'PUT')
{
    $input = $_GET;
    $postId = $input['ID_Venta'];
    $fields = getParams($input);

    $sql = "
          UPDATE venta
          SET $fields
          WHERE ID_Venta='$postId'
           ";

    $statement = $dbConn->prepare($sql);
    bindAllValues($statement, $input);

    $statement->execute();
    header("HTTP/1.1 200 OK");
    exit();
}


//En caso de que ninguna de las opciones anteriores se haya ejecutado
header("HTTP/1.1 400 Bad Request");

?>