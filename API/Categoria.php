<?php
include "config.php";
include "utils.php";


$dbConn =  connect($db);

/*
  listar todos los articulos o solo uno GET
 */
if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
    if (isset($_GET['ID_Categoria']))
    {
      //Mostrar un post
      $sql = $dbConn->prepare("SELECT * FROM categoria where ID_Categoria=:ID_Categoria");
      $sql->bindValue(':ID_Categoria', $_GET['ID_Categoria']);
      $sql->execute();
      header("HTTP/1.1 200 OK");
      echo json_encode(  $sql->fetch(PDO::FETCH_ASSOC)  );
      exit();
	  }
    else {
      //Mostrar lista de post
      $sql = $dbConn->prepare("SELECT * FROM categoria");
      $sql->execute();
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      header("HTTP/1.1 200 OK");
      echo json_encode( $sql->fetchAll()  );
      exit();
	}
}

// Crear un nuevo post
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $input = $_POST;
    $sql = "INSERT INTO categoria
          (Nombre, Descripcion)
          VALUES
          (:Nombre, :Descripcion)";
    $statement = $dbConn->prepare($sql);
    bindAllValues($statement, $input);
    $statement->execute();
    $postId = $dbConn->lastInsertId();
    if($postId)
    {
      $input['ID_Categoria'] = $postId;
      header("HTTP/1.1 200 OK");
      echo json_encode($input);
      exit();
	 }
}

//Borrar
if ($_SERVER['REQUEST_METHOD'] == 'DELETE')
{
	$id = $_GET['ID_Categoria'];
  $statement = $dbConn->prepare("DELETE FROM Articulo where ID_Categoria=:ID_Categoria");
  $statement->bindValue(':ID_Categoria', $id);
  $statement->execute();
	header("HTTP/1.1 200 OK");
	exit();
}

//Actualizar
if ($_SERVER['REQUEST_METHOD'] == 'PUT')
{
    $input = $_GET;
    $postId = $input['ID_Categoria'];
    $fields = getParams($input);

    $sql = "
          UPDATE categoria
          SET $fields
          WHERE ID_Categoria='$postId'
           ";

    $statement = $dbConn->prepare($sql);
    bindAllValues($statement, $input);

    $statement->execute();
    header("HTTP/1.1 200 OK");
    exit();
}


//En caso de que ninguna de las opciones anteriores se haya ejecutado
header("HTTP/1.1 400 Bad Request");

?>