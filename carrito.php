<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head>
<body>
<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Producto</th>
      <th scope="col">Cantidad</th>
      <th scope="col">Precio</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td><img src="michi/michi1.jpg"></td>
      <td>2</td>
      <td>10</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td><img src="michi/godzila.jpg"></td>
      <td>1</td>
      <td>12</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td><img src="michi/michinormal.jpg"></td>
      <td>3</td>
      <td>50</td>
    </tr>

  </tbody>

</table>
<button type="button" class="btn btn-dark">Seguir Comprando</button>
<button type="button" class="btn btn-dark">Completar Compra</button>
<button type="button" class="btn btn-dark">Borrar lista de Compra</button>
</body>
</html>