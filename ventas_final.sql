-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-03-2021 a las 04:30:12
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ventas_final`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE `articulo` (
  `ID_Articulo` int(11) NOT NULL,
  `ID_Categoria` int(11) NOT NULL,
  `Nombre_Articulo` varchar(50) NOT NULL DEFAULT '',
  `Precio_Articulo` float NOT NULL DEFAULT 0,
  `Cantidad` int(11) NOT NULL DEFAULT 0,
  `Descripcion` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`ID_Articulo`, `ID_Categoria`, `Nombre_Articulo`, `Precio_Articulo`, `Cantidad`, `Descripcion`) VALUES
(1, 1, 'Teléfono Samsung A5', 1800, 80, 'Celular Samsung A5: Ram 2G Almacenamiento 16GB'),
(2, 1, 'ASUS ROG G531GT-BI7N6', 100000, 52, '15.6\" FHD Gaming Ordenador portátil, Intel Hexa-Co'),
(3, 1, 'HP PAVILION GAMING', 12000.5, 45, 'Con el procesador Intel® Core™ i7 de diez núcleos de 9.ª generación y los gráficos dedicados'),
(4, 1, 'Xiamo black shark', 8000, 66, 'Xiaomi sigue los pasos de Razer, que hace unos meses abrió la veda de los smartphones orientados al gaming con su Razer Phone. Al contrario de lo que '),
(7, 1, 'Camara Nikon Reflex D7200', 1290000, 20, ' 18 – 140 mm VR & 70 – 300 mm lente macro Bundle Incluye: Deluxe Digital SLR gadget Bag 64 GB Class 10 UHS-'),
(8, 1, 'Razer Phone 2', 800, 36, 'RAM 8 GB LPDDR4X, Memoria 64 GB V4 UFS, Conectividad LTE, Wi‑Fi 802.11ac con MIMO 2x2, Bluetooth 5.0, NFC'),
(9, 1, 'MSI GL75 Gaming Laptop', 1149, 0, 'Core i7-9750H, 16 GB RAM, 17.3\" 144Hz pantalla Full HD, NVidia GTX 1660 Ti, 256 GB SSD + 1 TB HDD');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `ID_Categoria` int(11) NOT NULL,
  `Nombre` varchar(50) NOT NULL DEFAULT '',
  `Descripcion` varchar(50) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`ID_Categoria`, `Nombre`, `Descripcion`) VALUES
(1, 'Electronicos', 'Productos Electronicos'),
(2, 'Cocina', 'Articulos de Cocina');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingreso_articulos`
--

CREATE TABLE `ingreso_articulos` (
  `ID_Ingreso` int(11) NOT NULL,
  `ID_Articulo` int(11) NOT NULL,
  `ID_Provedor` int(11) NOT NULL,
  `Fecha` datetime NOT NULL,
  `Costo` float NOT NULL,
  `Cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provedor`
--

CREATE TABLE `provedor` (
  `ID_Provedor` int(11) NOT NULL,
  `Nombre_Provedor` varchar(50) NOT NULL,
  `Documento_Identidad` int(11) NOT NULL,
  `Direccion` varchar(50) NOT NULL,
  `Telefono` int(11) NOT NULL,
  `Email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `ID_Usuario` int(11) NOT NULL,
  `Rol` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `ID_Cliente` int(11) NOT NULL,
  `ID_Usuario` int(11) NOT NULL,
  `Nombre_Cliente` varchar(50) NOT NULL,
  `Documento_Identidad` int(11) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Telefono` int(11) NOT NULL,
  `Direccion` varchar(50) NOT NULL,
  `Contraseña` varchar(50) NOT NULL,
  `Numero_Tarjeta` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `ID_Venta` int(11) NOT NULL,
  `ID_Articulo` int(11) NOT NULL,
  `ID_Cliente` int(11) NOT NULL,
  `Fecha` datetime NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `Detalle_Venta` varchar(50) NOT NULL,
  `Precio_Total` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD PRIMARY KEY (`ID_Articulo`),
  ADD KEY `FK_articulo_categoria` (`ID_Categoria`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`ID_Categoria`);

--
-- Indices de la tabla `ingreso_articulos`
--
ALTER TABLE `ingreso_articulos`
  ADD PRIMARY KEY (`ID_Ingreso`),
  ADD KEY `FK_ingreso_articulos_articulo` (`ID_Articulo`),
  ADD KEY `FK_ingreso_articulos_provedor` (`ID_Provedor`);

--
-- Indices de la tabla `provedor`
--
ALTER TABLE `provedor`
  ADD PRIMARY KEY (`ID_Provedor`);

--
-- Indices de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD PRIMARY KEY (`ID_Usuario`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`ID_Cliente`),
  ADD KEY `FK_usuario_tipo_usuario` (`ID_Usuario`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`ID_Venta`),
  ADD KEY `FK_venta_articulo` (`ID_Articulo`),
  ADD KEY `FK_venta_cliente` (`ID_Cliente`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulo`
--
ALTER TABLE `articulo`
  MODIFY `ID_Articulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `ID_Categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ingreso_articulos`
--
ALTER TABLE `ingreso_articulos`
  MODIFY `ID_Ingreso` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `provedor`
--
ALTER TABLE `provedor`
  MODIFY `ID_Provedor` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  MODIFY `ID_Usuario` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `ID_Cliente` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `ID_Venta` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD CONSTRAINT `FK_articulo_categoria` FOREIGN KEY (`ID_Categoria`) REFERENCES `categoria` (`ID_Categoria`);

--
-- Filtros para la tabla `ingreso_articulos`
--
ALTER TABLE `ingreso_articulos`
  ADD CONSTRAINT `FK_ingreso_articulos_articulo` FOREIGN KEY (`ID_Articulo`) REFERENCES `articulo` (`ID_Articulo`),
  ADD CONSTRAINT `FK_ingreso_articulos_provedor` FOREIGN KEY (`ID_Provedor`) REFERENCES `provedor` (`ID_Provedor`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `FK_usuario_tipo_usuario` FOREIGN KEY (`ID_Usuario`) REFERENCES `tipo_usuario` (`ID_Usuario`);

--
-- Filtros para la tabla `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `FK_venta_articulo` FOREIGN KEY (`ID_Articulo`) REFERENCES `articulo` (`ID_Articulo`),
  ADD CONSTRAINT `FK_venta_cliente` FOREIGN KEY (`ID_Cliente`) REFERENCES `usuario` (`ID_Cliente`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
